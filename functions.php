<?php
/**
 * Child Starter functions and definitions
 *
 */
add_action('wp_head', function() {
	?>
	<link rel="icon" type="image/x-icon" href="/wp-content/themes/safe/favicon.svg">
	<link rel="stylesheet" href="https://i.icomoon.io/public/32fdff22a3/Safe/style.css">
	<script>
    !function(t,e){var o,n,p,r;e.__SV||(window.posthog=e,e._i=[],e.init=function(i,s,a){function g(t,e){var o=e.split(".");2==o.length&&(t=t[o[0]],e=o[1]),t[e]=function(){t.push([e].concat(Array.prototype.slice.call(arguments,0)))}}(p=t.createElement("script")).type="text/javascript",p.async=!0,p.src=s.api_host+"/static/array.js",(r=t.getElementsByTagName("script")[0]).parentNode.insertBefore(p,r);var u=e;for(void 0!==a?u=e[a]=[]:a="posthog",u.people=u.people||[],u.toString=function(t){var e="posthog";return"posthog"!==a&&(e+="."+a),t||(e+=" (stub)"),e},u.people.toString=function(){return u.toString(1)+".people (stub)"},o="capture identify alias people.set people.set_once set_config register register_once unregister opt_out_capturing has_opted_out_capturing opt_in_capturing reset isFeatureEnabled onFeatureFlags".split(" "),n=0;n<o.length;n++)g(u,o[n]);e._i.push([i,s,a])},e.__SV=1)}(document,window.posthog||[]);
    posthog.init('phc_X4RTrsfwl8oStLty0AH2sPpLiDkvMmuZ4KuzVpla5s2',{api_host:'https://eu.posthog.com'})
</script>
	<?php
  });

add_action( 'wp_enqueue_scripts', function() {
    wp_enqueue_style('theme-override', get_stylesheet_directory_uri() . '/static/base.min.css', array(), '1.0.0', 'all');
});

 add_action('wp_head', function() {
	$user = wp_get_current_user();
	if($user->exists()) {
		if(str_contains($user->user_email, '@imsmarketing.ie')) {
			?>
			<script>
			  window.markerConfig = {
				project: '646c6c197715f44b621dc4ab', 
				source: 'snippet',
				reporter: {
					email: 'vini@imsmarketing.ie',
					fullName: 'IMS Marketing',
				},
			  };
			</script>
			<?php
		} else {
			?>
			<script>
			  window.markerConfig = {
				project: '646c6c197715f44b621dc4ab', 
				source: 'snippet',
				reporter: {
					email: '<?= $user->user_email; ?>',
					fullName: '<?= $user->display_name; ?>',
				},
			  };
			</script>
			<?php
		}
		?>
		<script>
			!function(e,r,a){if(!e.__Marker){e.__Marker={};var t=[],n={__cs:t};["show","hide","isVisible","capture","cancelCapture","unload","reload","isExtensionInstalled","setReporter","setCustomData","on","off"].forEach(function(e){n[e]=function(){var r=Array.prototype.slice.call(arguments);r.unshift(e),t.push(r)}}),e.Marker=n;var s=r.createElement("script");s.async=1,s.src="https://edge.marker.io/latest/shim.js";var i=r.getElementsByTagName("script")[0];i.parentNode.insertBefore(s,i)}}(window,document);
		</script>
		<?php
	}
  });

  function override_iframe_template_styles_with_inline_styles() {
    wp_add_inline_style(
        /**
         *  Below, use 'give-sequoia-template-css' to style the Multi-Step form template,
         *  'give-styles' to style the donor dashboard, and 'give-classic-template' to style the Classic template
         */
        'give-classic-template',
        '
		.give-form-goal-progress {
			border:none !important;
		}
		.give-form-stats-panel-stat:first-child:after, .give-form-stats-panel-stat:nth-child(2):after {
			background-color: transparent !important;
		}
		.give-form-stats-panel-stat {
			background-color: #fff !important;
		}
		.give-donation-amount .give-text-input {
			color: #306F5E !important;
		}
		.give-donation-amount {
			border-color: #306F5E !important;
		}
		:root {
			--give-primary-font: "Poppins",sans-serif !important;
		}
        '
    );
}


add_action('wp_print_styles', 'override_iframe_template_styles_with_inline_styles', 10);
