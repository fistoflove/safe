<?php

use IMSWP\Helper\Fields;

$fields = new Fields(__DIR__);

$fields->register_tab(
    "Email",
    [
        ["Text", "link"]
    ]
);

$fields->register_tab(
    "Phone",
    [
        ["Text", "link"]
    ]
);

$fields->register_tab(
    "Location",
    [
        ["Text", "wysiwyg"]
    ]
);